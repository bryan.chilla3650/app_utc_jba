<br>
<div class="row">
    <div class="col-md-2 "> </div>
    <div class="col-md-8" style="border: 2px solid darkgrey;">
      <form action="<?php echo site_url(); ?>/clientes/guardarCliente" method="post" id="frm_nuevo_cliente">
        <br>
      <center> <h3>Nuevo Cliente</h3> </center>
            <br>

            <label for="">Identificacion:</label><br>
            <input type="number" name="identificador_cli" id="identificador_cli" class="form-control" placeholder="Ingrese su número de Cedula">
            <br><br>
            <label for="">Nombre:</label><br>
            <input type="text" class="form-control" name="nombre_cli" id="nombre_cli" placeholder="Ingrese su Nombre">
            <br><br>
            <label for="">Apellidos:</label><br>
            <input type="text" class="form-control" name="apellido_cli" id="apellido_cli" placeholder="Ingrese sus Apellidos">
            <br><br>
            <label for="">Direccion:</label><br>
            <input type="text" class="form-control" name="direccion_cli" id="direccion_cli" placeholder="Ingrese la direccion">
            <br><br>
                <label for="">Estado</label>
                  <select class="form-control" name="estado_cli" id="estado_cli" >
                      <option value="">Seleccione...</option>
                      <option value="Activo">Activo</option>
                      <option value="Inactivo">Inactivo</option>
                  </select><br><br>
            <button type="submit" class="btn btn-info" name="button"> <i class="fa-solid fa-floppy-disk"></i> Registrar</button>
            &nbsp;&nbsp;&nbsp
            <a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
           <br><br>
      </form>
    </div>
    <div class="col-md-2"> </div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_cliente").validate({
      rules:{
        identificacion_cli:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_cli:{
          required:true,
          letras:true
        },
        apellido_cli:{
          required:true,
          letras:true
        },
        direccion_cli:{
          required:true,
          letras:true

        estado_cli:{
          required:true
        }

      },
      messages:{
        identificacion_cli:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        apellido_cli:{
          required:"por favor ingrese el apellido",
          letras:"solo se acepta letras"
        },
        nombre_cli:{
          required:"por favor ingrese el nombre",
          letras:"solo se acepta letras"
        },
        direccion_cli:{
          required:"por favor ingrese una direccion",
          letras:"solo se acepta letras"
        },

        estado_cli:{
          required:"por favor seleccione un estado"
        }
      }
    });
</script>
