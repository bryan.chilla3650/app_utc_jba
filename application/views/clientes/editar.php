<div class="row">
<div class="col-md-2 "> </div>
<div class="col-md-8">
  <center>
    <h1> Editar Cliente</h1>
    <br>
    <hr>
    <br>
</center>
<form action="<?php echo site_url(); ?>/clientes/procesarActualizacion" method="post">

<input class="form-control" type="hidden"  readonly name="id_cli" id="id_cli" value="<?php echo $cliente->id_cli; ?>">
    <br>
    <br>
    <label  for="">IDENTIFICACIÓN</label>
    <input class="form-control"type="number" name="identificador_cli" id="identificador_cli" placeholder="Por favor Ingrese la Identificacion" value="<?php echo $cliente->identificador_cli; ?>">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" type="text" name="nombre_cli" id="nombre_cli" placeholder="Por favor Ingrese el nombre"value="<?php echo $cliente->nombre_cli; ?>">
    <br>
    <br>
    <label  for="">APELLIDO</label>
    <input class="form-control" type="text" name="apellido_cli" id="apellido_cli" placeholder="Por favor Ingrese el apellido" value="<?php echo $cliente->apellido_cli; ?>">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" type="text" name="direccion_cli" id="direccion_cli" placeholder="Por favor Ingrese la dirección"value="<?php echo $cliente->direccion_cli; ?>">
    <br>
    <br>
    <label for="">ESTADO</label>

    <select class="form-control" name="estado_cli" id="estado_cli">
    <option value="">Selecione...</option>
        <option value="Ativo">Activo</option>
        <option value="Inactivo">Inactivo</option>
    </select>

    <br>
    <br>
    <div align="center" >
    <button class="btn btn-success" type="submit" name="button"> <i class="fa-solid fa-floppy-disk"></i>
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp
    <button class="btn btn-danger" type="submit" name="button">
    <a href="<?php echo site_url();?>/clientes/index"> <strong style="color:white;"><i class="fa fa-times"></i> CANCELAR</strong></a></button>
    </div>




</form>
</div>
<div class="col-md-2 "> </div>
</div>


<script type="text/javascript">
    $("#estado_cli").val("<?php echo $cliente->estado_cli; ?>");
</script>
