<br>
<center>
<h3>Listado de Clientes</h3>
<hr>
<a href="<?php echo site_url(); ?>/clientes/nuevo" class="btn btn-info"> <i class="fa fa-plus"></i> Agregar Nuevo </a>


<?php if ($listadoClientes): ?>
  <table class="table table-bordered table-striped table-hover">
      <thead>
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">IDENTIFICACION</th>
          <th class="text-center">NOMBRE</th>
          <th class="text-center">APELLIDO</th>
          <th class="text-center">DIRECCION</th>
          <th class="text-center">ESTADO</th>
        </tr>
      </thead>

      <tbody>
          <?php foreach ($listadoClientes->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                  <?php echo $filaTemporal->id_cli; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->identificador_cli; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->nombre_cli; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->apellido_cli; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->direccion_cli; ?>
              </td>
              <td class="text-center">
              <?php if ($filaTemporal->estado_cli=="Activo"): ?>
                  <div class="alert alert-success">Activo</div>
              <?php else: ?>
                  <div class="alert alert-danger">Inactivo</div>
              <?php endif; ?>
              </td>
              <td class="text-center">
                <a class="btn btn-warning" href="<?php echo site_url();
                ?>/clientes/editar/<?php echo $filaTemporal->id_cli; ?>"><strong style="color:white;"> <i class="fa fa-pen"></i> </strong></a>

                <a class="btn btn-danger" href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_cli; ?>')" ><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
              </td>
            </tr>
          <?php endforeach; ?>
      </tbody>
      <?php else: ?>
        <div class="alert alert-danger">
            <h3>No se encontraron registros</h3>
        </div>
  </table>
  <?php endif; ?>
</center>

<script type="text/javascript">
    function confirmarEliminacion(id_cli){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el cliente de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/clientes/procesarEliminacion/"+id_cli;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
