<div class="row">
<div class="col-md-2 "> </div>
<div class="col-md-8">
  <center>
    <h1> Editar Sucursal</h1>
    <br>
    <hr>
    <br>
</center>
<form action="<?php echo site_url(); ?>/sucursales/procesarActualizacion" method="post">

<input class="form-control" type="hidden"  readonly name="id_suc" id="id_suc" value="<?php echo $sucursal->id_suc; ?>">
    <br>
    <br>
    <label  for="">IDENTIFICACIÓN</label>
    <input class="form-control"type="number" name="identificador_suc" id="identificador_suc" placeholder="Por favor Ingrese la Identificacion" value="<?php echo $sucursal->identificador_suc; ?>">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" type="text" name="nombre_suc" id="nombre_suc" placeholder="Por favor Ingrese el nombre"value="<?php echo $sucursal->nombre_suc; ?>">
    <br>
    <br>
    <label  for="">Encargado</label>
    <input class="form-control" type="text" name="encargado_suc" id="encargado_suc" placeholder="Por favor Ingrese el nombre del encargado" value="<?php echo $sucursal->encargado_suc; ?>">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" type="text" name="direccion_suc" id="direccion_suc" placeholder="Por favor Ingrese la dirección"value="<?php echo $sucursal->direccion_suc; ?>">
    <br>
    <br>
    <label for="">ESTADO</label>

    <select class="form-control" name="estado_suc" id="estado_suc">
    <option value="">Selecione...</option>
        <option value="Activo">Activo</option>
        <option value="Inactivo">Inactivo</option>
    </select>

    <br>
    <br>
    <div align="center" >
    <button class="btn btn-success" type="submit" name="button">
      <i class="fa-solid fa-floppy-disk"></i> ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp
    <button class="btn btn-danger" type="submit" name="button">
    <a href="<?php echo site_url();?>/sucursales/index"> <strong style="color:white;"><i class="fa fa-times"></i> CANCELAR</strong></a></button>
    </div>




</form>
</div>
<div class="col-md-2 "> </div>
</div>


<script type="text/javascript">
    $("#estado_suc").val("<?php echo $sucursal->estado_suc; ?>");
</script>
