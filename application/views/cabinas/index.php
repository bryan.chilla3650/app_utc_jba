<br>
<center>
  <h2>FORMULARIO CABINAS</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/cabinas/metal">Agregar Registro de Cabinas</a>
</center>

<?php if ($listadoCabinas): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">MARCA</th>
        <th class="text-center">COLOR</th>
        <th class="text-center">TIPO</th>


      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoCabinas->result() as $filaTemporal): ?>
        <tr>

          <td class="text-center">
            <?php echo $filaTemporal->id_cabina; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->nombre_cab; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->marca_cab; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->color_cab; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->tipo_cab; ?>
          </td>

          <!--OPCIONES EDITAR Y ELIMINAR-->
          <td class="text-center">
          <a class="btn btn-success"  href="<?php echo site_url(); ?>/cabinas/editar/<?php echo $filaTemporal->id_cabina; ?>" ><i class="fa fa-pen"></i></a>
          <a class="btn btn-danger"  href="<?php echo site_url(); ?>/cabinas/procesarEliminacion/<?php echo $filaTemporal->id_cabina; ?>" onclick="return confirm('¿Esta seguro?')"><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
          </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON CABINAS REGISTRADAS</h1>
  </div>
<?php endif; ?>
