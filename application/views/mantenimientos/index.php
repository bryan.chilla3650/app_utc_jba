<br>
<center>
  <h2>FORMULARIO DE MANTENIMIENTO</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/mantenimientos/repuesto">Agregar Nombre de la clase de manteniemiento</a>
</center>

<?php if ($listadoMantenimientos): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE MANTENIMIENTO</th>
        <th class="text-center">REPUESTO</th>
        <th class="text-center">ACEITE</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoMantenimientos->result() as $filaTemporal): ?>
        <tr>

          <td class="text-center">
            <?php echo $filaTemporal->id_man; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->tipo_man; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->repuestos_man; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->aceite_man; ?>
          </td>


          <!--OPCIONES EDITAR Y ELIMINAR-->
          <td class="text-center">
          <a class="btn btn-success"  href="<?php echo site_url(); ?>/mantenimientos/editar/<?php echo $filaTemporal->id_man; ?>" ><i class="fa fa-pen"></i></a>
            <a class="btn btn-danger"  href="<?php echo site_url(); ?>/mantenimientos/procesarEliminacion/<?php echo $filaTemporal->id_man; ?>" onclick="return confirm('¿Esta seguro?')"><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
          </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON LLANTAS REGISTRADOS</h1>
  </div>
<?php endif; ?>
