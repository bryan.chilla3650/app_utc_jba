<div class="col-md-12">
<div  class="bg-white text-black">
		<div class="card bg-white">
			<div class="card-header" > Registro para cotizar </div>
			<div class="card-body">
				<form  class="form-control input-sm required"action="<?php echo Site_url();?>/sports/procesarActualizacion"
					method="post">
          	<input type="hidden" name="id_sp" id="id_sp" value="<?php echo $sport->id_sp; ?>">
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">NOMBRE </label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="nombre_sp" id="nombre_sp"
                class="form-control form-control-sm" placeholder="ingrese nombre" value="<?php echo $sport->nombre_sp; ?>" required >
              <small class="form-text text-white"
              ></small>
            </div>
          </div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">APELLIDO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="text" name="apellido_sp" id="apellido_sp"
								class="form-control form-control-sm" placeholder="ingrese el apellido" value="<?php echo $sport->apellido_sp; ?>" required  >
							<small class="form-text text-danger"
							></small>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">TELEFONO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="number" name="telefono_sp" id="telefono_sp"
								class="form-control form-control-sm"placeholder="ingrese telefono" value="<?php echo $sport->telefono_sp; ?>" required pattern="[A-Za-z]+" >
							<small class="form-text text-danger"
							 ></small>
						</div>
					</div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">EMAIL</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="email_sp" id="email_sp"
                class="form-control form-control-sm" placeholder="ingrese el email" value="<?php echo $sport->email_sp; ?>" required  >
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">CIUDAD</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="ciudad_sp" id="ciudad_sp"
                class="form-control form-control-sm" placeholder="ingrese la ciudad " value="<?php echo $sport->ciudad_sp; ?>" required  >
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">AGENCIA</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="agencia_sp" id="agencia_sp"
                class="form-control form-control-sm" placeholder="ingrese la agencia" value="<?php echo $sport->agencia_sp; ?>" required  >
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          </div>
				<div class="row">
          <div class="col-md-6">
            <div class="col-md-12">
              <center>
                <button class="btn btn-primary" type="submit" name="button"> ACTUALIZAR</button>

              </center>
              <br>
            </div>
          </div>
          <div class="col-md-6">
            <center>
           <a href="<?php echo site_url(); ?>/sport/indexx" class="btn btn-warning">Cancelar</a>
         </center>
          </div>
        </div>


				</form>
			</div>
		</div>
	</div>
</div>
