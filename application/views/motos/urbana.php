<div class="col-md-12">
<div  class="bg-white text-black">
		<div class="card bg-white">
			<div class="card-header" > Registro para cotizar </div>
			<div class="card-body">
				<form  action="<?php echo Site_url();?>/motos/guardarMoto"
					method="post">

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">NOMBRE</label>
            <div class="col-sm-6" >
              <input class="form-control bg-white"    type="text" name="nombre_mot" id="nombre_mot"
                placeholder="Ingrese nombre" required  />
            </div>
          </div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">EMAIL</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="email" name="email_mot" id="email_mot"
								class="form-control form-control-sm" placeholder="Ingrese  email"  />
							<small class="form-text text-danger"
							></small>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">REGION</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="number" name="region_mot" id="region_mot"
								class="form-control form-control-sm"placeholder="Ingrese region" required pattern="[A-Za-z]+ "/>
							<small class="form-text text-danger"
							 ></small>
						</div>
					</div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">COLOR</label>
            <div class="col-sm-6">
              <input type="text" name="color_mot" id="color_mot"
              class="form-control bg-white" placeholder="Ingrese color" required />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">TELEFONO</label>
            <div class="col-sm-6">
              <input type="number" name="telefono_mot" id="telefono_mot"
                class="form-control bg-white" placeholder="Ingrese telefono" required pattern="[A-Za-z]+"  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>

          </div>
				<div class="row">
          <div class="col-md-6">
            <div class="col-md-12">
              <center>
                <input type="submit" value="Guardar solicitud"
                  class="btn btn-primary" />
              </center>
              <br>
            </div>
          </div>
          <div class="col-md-6">
            <center>
           <a href="<?php echo site_url(); ?>/motos/indexx" class="btn btn-warning"><i class="fa fa-circle-minus"></i>Cancelar</a>
         </center>
          </div>
        </div>


				</form>
			</div>
		</div>
	</div>
</div>
