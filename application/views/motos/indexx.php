<br>
<center>
<h3>Listado urbanas</h3>
<hr>
<a href="<?php echo site_url(); ?>/motos/index" class="btn btn-info"><i class="fa fa-plus"></i> Agregar Nueva cotizacion</a>


<?php if ($listadoMotos): ?>
  <table class="table table-bordered table-striped table-hover">
      <thead>
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">NOMBRE</th>
          <th class="text-center">EMAIL</th>
          <th class="text-center">REGION</th>
          <th class="text-center">COLOR</th>
          <th class="text-center">TELEFONO</th>

        </tr>
      </thead>

      <tbody>
          <?php foreach ($listadoMotos->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                  <?php echo $filaTemporal->id_moto; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->nombre_mot; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->email_mot; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->region_mot; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->color_mot; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->telefono_mot; ?>
              </td>

              <td class="text-center">
                <a class="btn btn-warning" href="<?php echo site_url();
                ?>/motos/editar/<?php echo $filaTemporal->id_moto; ?>"><i class="fa fa-pen"></i></a>

                <a class="btn btn-danger"  href="<?php echo site_url();
                ?>/motos/procesarEliminacion/<?php echo $filaTemporal->id_moto; ?>" onclick="return confirm('¿Esta seguro?')"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
          <?php endforeach; ?>
      </tbody>
      <?php else: ?>
        <div class="alert alert-danger">
            <h3>No se encontraron registros</h3>
        </div>
  </table>
  <?php endif; ?>
</center>
