<div class="col-md-12">
<div  class="bg-white text-black">
		<div class="card bg-white">
			<div class="card-header" > Registro para cotizar </div>
			<div class="card-body">
				<form  class="form-control input-sm required"action="<?php echo Site_url();?>/nakes/procesarActualizacion"
					method="post">
						<input type="hidden" name="id_na" id="id_na" value="<?php echo $nake->id_na; ?>">
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">NOMBRE</label>
            <div class="col-sm-6" >
              <input class="form-control bg-white" type="text" name="nombre_na" id="nombre_na"
                class="form-control form-control-sm" placeholder="ingrese nombre" value="<?php echo $nake->nombre_na; ?>" required />
              <small class="form-text text-white"
              ></small>
            </div>
          </div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">APELLIDO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="text" name="apellido_na" id="apellido_na"
								class="form-control form-control-sm" placeholder="ingrese el apellido" value="<?php echo $nake->apellido_na; ?>" required  />
							<small class="form-text text-danger"
							></small>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">TELEFONO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="number" name="telefono_na" id="telefono_na"
								class="form-control form-control-sm"placeholder="ingrese telefono" value="<?php echo $nake->telefono_na; ?>" required pattern="[A-Za-z]+"/>
							<small class="form-text text-danger"
							 ></small>
						</div>
					</div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">EMAIL</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="email_na" id="email_na"
                class="form-control form-control-sm" placeholder="ingrese el email" value="<?php echo $nake->email_na; ?>" required  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">CIUDAD</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="ciudad_na" id="ciudad_na"
                class="form-control form-control-sm" placeholder="ingrese la ciudad " value="<?php echo $nake->ciudad_na; ?>" required  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">AGENCIA</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="agencia_na" id="agencia_na"
                class="form-control form-control-sm" placeholder="ingrese la agencia" value="<?php echo $nake->agencia_na; ?>" required  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          </div>
				<div class="row">
          <div class="col-md-6">
            <div class="col-md-12">
              <center>
                <button class="btn btn-primary" type="submit" name="button"> ACTUALIZAR</button>

              </center>
              <br>
            </div>
          </div>
          <div class="col-md-6">
            <center>
           <a href="<?php echo site_url(); ?>/nakes/indexx" class="btn btn-warning">Cancelar</a>
         </center>
          </div>
        </div>


				</form>
			</div>
		</div>
	</div>
</div>
