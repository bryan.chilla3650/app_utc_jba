<br>
<center>
<h3>Listado naked</h3>
<hr>
<a href="<?php echo site_url(); ?>/nakes/index" class="btn btn-info"><i class="fa fa-plus"></i> Agregar Nueva cotizacion</a>


<?php if ($listadoNakes): ?>
  <table class="table table-bordered table-striped table-hover">
      <thead>
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">NOMBRE</th>
          <th class="text-center">APELLIDO</th>
          <th class="text-center">TELEFONO</th>
          <th class="text-center">EMAIL</th>
          <th class="text-center">CIUDAD</th>
            <th class="text-center">AGENCIA</th>

        </tr>
      </thead>

      <tbody>
          <?php foreach ($listadoNakes->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                  <?php echo $filaTemporal->id_na; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->nombre_na; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->apellido_na; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->telefono_na; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->email_na; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->ciudad_na; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->agencia_na; ?>
              </td>
              <td class="text-center">
                <a class="btn btn-warning" href="<?php echo site_url();
                ?>/nakes/editar/<?php echo $filaTemporal->id_na; ?>"><i class="fa fa-pen"></i></a>

                <a class="btn btn-danger"  href="<?php echo site_url();
                ?>/nakes/procesarEliminacion/<?php echo $filaTemporal->id_na; ?>" onclick="return confirm('¿Esta seguro?')"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
          <?php endforeach; ?>
      </tbody>
      <?php else: ?>
        <div class="alert alert-danger">
            <h3>No se encontraron registros</h3>
        </div>
  </table>
  <?php endif; ?>
</center>
