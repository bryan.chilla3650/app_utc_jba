<br>
<div class="row">
    <div class="col-md-2 "> </div>
    <div class="col-md-8" style="border: 2px solid darkgrey;">
      <form action="<?php echo site_url(); ?>/vehiculos/guardarVehiculo" method="post">
        <br>
      <center> <h3>FORMULARIO DE REGISTRO</h3> </center>
            <br>

            <label for="">Identificacion:</label><br>
            <input type="number" name="identificador_veh" id="identificador_veh" class="form-control" placeholder="Ingrese su número de chasis" Required>
            <br><br>
            <label for="">Nombre:</label><br>
            <input type="text" class="form-control" name="nombre_veh" id="nombre_veh" placeholder="Ingrese el nombre del vehiculo" Required>
            <br><br>
            <label for="">Color</label>
              <select class="form-control" name="color_veh" id="color_veh" Required>
                  <option value="">Seleccione...</option>
                  <option value="Azul">Azul</option>
                  <option value="Negro">Negro</option>
                  <option value="Blanco">Blanco</option>
                  <option value="Rojo">Rojo</option>
              </select><br><br>
              <label for="">Tipo</label>
                <select class="form-control" name="tipo_veh" id="tipo_veh" Required>
                    <option value="">Seleccione...</option>
                    <option value="Camioneta">Camioneta</option>
                    <option value="Automovil">Automovil</option>
                    <option value="SUV">SUV</option>
                </select><br><br>
                <label for="">Estado</label>
                  <select class="form-control" name="estado_veh" id="estado_veh" Required>
                      <option value="">Seleccione...</option>
                      <option value="Activo">Activo</option>
                      <option value="Inactivo">Inactivo</option>
                  </select><br><br>
            <button type="submit" class="btn btn-info" name="button"><i class="fa-solid fa-floppy-disk"></i> Registrar</button>
            &nbsp;&nbsp;&nbsp
            <a href="<?php echo site_url(); ?>/vehiculos/index" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
           <br><br>
      </form>
    </div>
    <div class="col-md-2"> </div>
</div>
