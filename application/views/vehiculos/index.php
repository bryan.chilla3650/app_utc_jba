<br>
<center>
<h3>Listado de Vehiculos</h3>
<hr>
<a href="<?php echo site_url(); ?>/vehiculos/nuevo" class="btn btn-info"><i class="fa fa-plus"></i> Agregar Nuevo </a>


<?php if ($listadoVehiculos): ?>
  <table class="table table-bordered table-striped table-hover">
      <thead>
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">IDENTIFICACION</th>
          <th class="text-center">NOMBRE</th>
          <th class="text-center">COLOR</th>
          <th class="text-center">TIPO</th>
          <th class="text-center">ESTADO</th>
        </tr>
      </thead>

      <tbody>
          <?php foreach ($listadoVehiculos->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                  <?php echo $filaTemporal->id_veh; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->identificador_veh; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->nombre_veh; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->color_veh; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->tipo_veh; ?>
              </td>
              <td class="text-center">
              <?php if ($filaTemporal->estado_veh=="Activo"): ?>
                  <div class="alert alert-success">Activo</div>
              <?php else: ?>
                  <div class="alert alert-danger">Inactivo</div>
              <?php endif; ?>
              </td>
              <td class="text-center">
                <a class="btn btn-warning" href="<?php echo site_url();
                ?>/vehiculos/editar/<?php echo $filaTemporal->id_veh; ?>"><strong style="color:white;"> <i class="fa fa-pen"></i></strong></a>

                <a class="btn btn-danger"  href="<?php echo site_url();
                ?>/vehiculos/procesarEliminacion/<?php echo $filaTemporal->id_veh; ?>" onclick="return confirm('¿Esta seguro que desea eliminar?')"><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
              </td>
            </tr>
          <?php endforeach; ?>
      </tbody>
      <?php else: ?>
        <div class="alert alert-danger">
            <h3>No se encontraron registros</h3>
        </div>
  </table>
  <?php endif; ?>
</center>
