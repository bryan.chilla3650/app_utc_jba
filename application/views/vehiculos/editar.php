<div class="row">
<div class="col-md-2 "> </div>
<div class="col-md-8">
  <center>
    <h1> Actualizar Vehiculo</h1>
    <br>
    <hr>
    <br>
</center>
<form action="<?php echo site_url(); ?>/vehiculos/procesarActualizacion" method="post">

<input class="form-control" type="hidden"  readonly name="id_veh" id="id_veh" value="<?php echo $vehiculo->id_veh; ?>">
    <br>
    <br>
    <label  for="">IDENTIFICACIÓN</label>
    <input class="form-control"type="number" name="identificador_veh" id="identificador_veh" placeholder="Por favor Ingrese la Identificacion" value="<?php echo $vehiculo->identificador_veh; ?>">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" type="text" name="nombre_veh" id="nombre_veh" placeholder="Por favor Ingrese el nombre"value="<?php echo $vehiculo->nombre_veh; ?>">
    <br>
    <br>
    <label for="">Color</label>
      <select class="form-control" name="color_veh" id="color_veh" >
          <option value="">Seleccione...</option>
          <option value="Azul">Azul</option>
          <option value="Negro">Negro</option>
          <option value="Blanco">Blanco</option>
          <option value="Rojo">Rojo</option>
      </select><br><br>
      <label for="">Tipo</label>
        <select class="form-control" name="tipo_veh" id="tipo_veh" >
            <option value="">Seleccione...</option>
            <option value="Camioneta">Camioneta</option>
            <option value="Automovil">Automovil</option>
            <option value="SUV">SUV</option>
        </select><br><br>
    <label for="">ESTADO</label>

    <select class="form-control" name="estado_veh" id="estado_veh">
    <option value="">Selecione...</option>
        <option value="Activo">Activo</option>
        <option value="Inactivo">Inactivo</option>
    </select>

    <br>
    <br>
    <div align="center" >
    <button class="btn btn-success" type="submit" name="button">
      <i class="fa-solid fa-floppy-disk"></i> ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp
    <button class="btn btn-danger" type="submit" name="button">
    <a href="<?php echo site_url();?>/vehiculos/index"> <strong style="color:white;"><i class="fa fa-times"></i> CANCELAR</strong></a></button>
    </div>




</form>
</div>
<div class="col-md-2 "> </div>
</div>


<script type="text/javascript">
    $("#color_veh").val("<?php echo $vehiculo->color_veh; ?>");
    $("#tipo_veh").val("<?php echo $vehiculo->tipo_veh; ?>");
    $("#estado_veh").val("<?php echo $vehiculo->estado_veh; ?>");
</script>
