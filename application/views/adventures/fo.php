<div class="col-md-12">
<div  class="bg-white text-black">
		<div class="card bg-white">
			<div class="card-header" > Registro para cotizar </div>
			<div class="card-body">
				<form  class="form-control input-sm required"action="<?php echo Site_url();?>/adventures/guardarAdventure"
					method="post">
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">NOMBRE</label>
            <div class="col-sm-6" >
              <input class="form-control bg-white" type="text" name="nombre_ad" id="nombre_ad"
                class="form-control form-control-sm" placeholder="ingrese nombre" required />
              <small class="form-text text-white"
              ></small>
            </div>
          </div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">APELLIDO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="text" name="apellido_ad" id="apellido_ad"
								class="form-control form-control-sm" placeholder="ingrese el apellido" required  />
							<small class="form-text text-danger"
							></small>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">TELEFONO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="number" name="telefono_ad" id="telefono_ad"
								class="form-control form-control-sm"placeholder="ingrese telefono" required pattern="[A-Za-z]+"/>
							<small class="form-text text-danger"
							 ></small>
						</div>
					</div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">EMAIL</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="email" name="email_ad" id="email_ad"
                class="form-control form-control-sm" placeholder="ingrese email"  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">CIUDAD</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="ciudad_ad" id="ciudad_ad"
                class="form-control form-control-sm" placeholder="ingrese la ciudad" required  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">AGENCIA</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="agencia_ad" id="agencia_ad"
                class="form-control form-control-sm" placeholder="ingrese la agencia" required  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          </div>
				<div class="row">
          <div class="col-md-6">
            <div class="col-md-12">
              <center>
                <input type="submit" value="Guardar solicitud"
                  class="btn btn-primary" />
              </center>
              <br>
            </div>
          </div>
          <div class="col-md-6">
            <center>
           <a href="<?php echo site_url(); ?>/adventures/indexx" class="btn btn-warning"><i class="fa fa-circle-minus"></i>Cancelar</a>
         </center>
          </div>
        </div>


				</form>
			</div>
		</div>
	</div>
</div>
