<div class="col-md-12">
<div  class="bg-white text-black">
		<div class="card bg-white">
			<div class="card-header" > Registro para cotizar </div>
			<div class="card-body">
				<form  class="form-control input-sm required"action="<?php echo Site_url();?>/adventures/procesarActualizacion"
					method="post">
					<input type="hidden" name="id_ad" id="id_ad" value="<?php echo $adventure->id_ad; ?>">
					<br>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">NOMBRE</label>
            <div class="col-sm-6" >
              <input class="form-control bg-white" type="text" name="nombre_ad" id="nombre_ad"
                class="form-control form-control-sm" placeholder="ingrese nombre" value="<?php echo $adventure->nombre_ad; ?>" required />
              <small class="form-text text-white"
              ></small>
            </div>
          </div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">APELLIDO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="text" name="apellido_ad" id="apellido_ad"
								class="form-control form-control-sm" placeholder="ingrese el apellido" value="<?php echo $adventure->apellido_ad; ?>" required  />
							<small class="form-text text-danger"
							></small>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">TELEFONO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="number" name="telefono_ad" id="telefono_ad"
								class="form-control form-control-sm"placeholder="ingrese telefono" value="<?php echo $adventure->telefono_ad; ?>" required pattern="[A-Za-z]+"/>
							<small class="form-text text-danger"
							 ></small>
						</div>
					</div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">EMAIL</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="email" name="email_ad" id="email_ad"
                class="form-control form-control-sm" placeholder="ingrese email" value="<?php echo $adventure->email_ad; ?>" />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">CIUDAD</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="ciudad_ad" id="ciudad_ad"
                class="form-control form-control-sm" placeholder="ingrese la ciudad" value="<?php echo $adventure->ciudad_ad; ?>" required  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">AGENCIA</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="agencia_ad" id="agencia_ad"
                class="form-control form-control-sm" placeholder="ingrese la agencia" value="<?php echo $adventure->agencia_ad; ?>" required  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          </div>
				<div class="row">
          <div class="col-md-6">
            <div class="col-md-12">
              <center>
                <button class="btn btn-primary" type="submit" name="button"> ACTUALIZAR</button>

              </center>
              <br>
            </div>
          </div>
          <div class="col-md-6">
            <center>
           <a href="<?php echo site_url(); ?>/adventures/indexx" class="btn btn-warning">Cancelar</a>
         </center>
          </div>
        </div>


				</form>
			</div>
		</div>
	</div>
</div>
