<br>
<center>
  <h2>FORMULARIO FURGON</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/furgones/tipo">Agregar Nuevo Furgon</a>
</center>

<?php if ($listadoFurgones): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE DEL TIPO DE FURGON</th>
        <th class="text-center">MARCA DEL CAMION</th>
        <th class="text-center">COLOR CAMION</th>
        <th class="text-center">TIPO CAMION</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoFurgones->result() as $filaTemporal): ?>
        <tr>

          <td class="text-center">
            <?php echo $filaTemporal->id_fur; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->nombre_fur; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->marca_fur; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->color_fur; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->tipo_fur; ?>
          </td>


          <!--OPCIONES EDITAR Y ELIMINAR-->
          <td class="text-center">
              <a class="btn btn-success"  href="<?php echo site_url(); ?>/furgones/editar/<?php echo $filaTemporal->id_fur; ?>" ><i class="fa fa-pen"></i></a>
            <a class="btn btn-danger"  href="<?php echo site_url(); ?>/furgones/procesarEliminacion/<?php echo $filaTemporal->id_fur; ?>" onclick="return confirm('¿Esta seguro?')"><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
          </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON FURGONES REGISTRADOS</h1>
  </div>
<?php endif; ?>
