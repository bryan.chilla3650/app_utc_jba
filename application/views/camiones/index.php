<br>
<center>
  <h2>FORMULARIO CAMION</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/camiones/pesado">Agregar Nuevo Camion</a>
</center>

<?php if ($listadoCamiones): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE DEL CAMION</th>
        <th class="text-center">MARCA DEL CAMION</th>
        <th class="text-center">TONELAJE CAMION</th>
        <th class="text-center">COLOR CAMION</th>
        <th class="text-center">TIPO CAMION</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoCamiones->result() as $filaTemporal): ?>
        <tr>

          <td class="text-center">
            <?php echo $filaTemporal->id_cam; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->nombre_cam; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->marca_cam; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->tonelaje_cam; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->tipo_cam; ?>
          </td>


          <!--OPCIONES EDITAR Y ELIMINAR-->
          <td class="text-center">
          <a class="btn btn-success"  href="<?php echo site_url(); ?>/camiones/editar/<?php echo $filaTemporal->id_cam; ?>" ><i class="fa fa-pen"></i></a>
            <a class="btn btn-danger"  href="<?php echo site_url(); ?>/camiones/procesarEliminacion/<?php echo $filaTemporal->id_cam; ?>" onclick="return confirm('¿Esta seguro?')"><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>

          </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON CAMIONES REGISTRADOS</h1>
  </div>
<?php endif; ?>
