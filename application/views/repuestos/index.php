<br>
<center>
<h3>Lista de Repuestos</h3>
<hr>
<a href="<?php echo site_url(); ?>/repuestos/nuevo" class="btn btn-info"> <i class="fa fa-plus"></i> Agregar Nuevo </a>


<?php if ($listadoRepuestos): ?>
  <table class="table table-bordered table-striped table-hover">
      <thead>
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">IDENTIFICACION</th>
          <th class="text-center">NOMBRE</th>
          <th class="text-center">DDESCRIPCION</th>
          <th class="text-center">ESTADO</th>
        </tr>
      </thead>

      <tbody>
          <?php foreach ($listadoRepuestos->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                  <?php echo $filaTemporal->id_rep; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->identificador_rep; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->nombre_rep; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->descripcion_rep; ?>
              </td>
              <td class="text-center">
              <?php if ($filaTemporal->estado_rep=="Activo"): ?>
                  <div class="alert alert-success">Activo</div>
              <?php else: ?>
                  <div class="alert alert-danger">Inactivo</div>
              <?php endif; ?>
              </td>
              <td class="text-center">
                <a class="btn btn-warning" href="<?php echo site_url();
                ?>/repuestos/editar/<?php echo $filaTemporal->id_rep; ?>"><strong style="color:white;"> <i class="fa fa-pen"></i> </strong></a>

                <a class="btn btn-danger"  href="<?php echo site_url();
                ?>/repuestos/procesarEliminacion/<?php echo $filaTemporal->id_rep; ?>" onclick="return confirm('¿Esta seguro?')"><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
              </td>
            </tr>
          <?php endforeach; ?>
      </tbody>
      <?php else: ?>
        <div class="alert alert-danger">
            <h3>No se encontraron registros</h3>
        </div>
  </table>
  <?php endif; ?>
</center>
