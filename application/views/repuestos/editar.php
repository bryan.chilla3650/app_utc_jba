<div class="row">
<div class="col-md-2 "> </div>
<div class="col-md-8">
  <center>
    <h1> Editar Informacion de Repuesto</h1>
    <br>
    <hr>
    <br>
</center>
<form action="<?php echo site_url(); ?>/repuestos/procesarActualizacion" method="post">

    <input class="form-control" type="hidden"  readonly name="id_rep" id="id_rep" value="<?php echo $repuesto->id_rep; ?>">
    <br>
    <br>
    <label  for="">IDENTIFICACIÓN</label>
    <input class="form-control"type="number" name="identificador_rep" id="identificador_rep" placeholder="Por favor Ingrese el numero de serie" value="<?php echo $repuesto->identificador_rep; ?>">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" type="text" name="nombre_rep" id="nombre_rep" placeholder="Por favor Ingrese el nombre"value="<?php echo $repuesto->nombre_rep; ?>">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" type="text" name="descripcion_rep" id="descripcion_rep" placeholder="Por favor Ingrese la descripcion"value="<?php echo $repuesto->descripcion_rep; ?>">
    <br>
    <br>
    <label for="">ESTADO</label>
    <select class="form-control" name="estado_rep" id="estado_rep">
    <option value="">Selecione...</option>
        <option value="Activo">Activo</option>
        <option value="Inactivo">Inactivo</option>
    </select>

    <br>
    <br>
    <div align="center" >
    <button class="btn btn-success" type="submit" name="button"> <i class="fa-solid fa-floppy-disk"></i>
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp
    <button class="btn btn-danger" type="submit" name="button">
    <a href="<?php echo site_url();?>/repuestos/index"> <strong style="color:white;"> <i class="fa fa-times"></i> CANCELAR</strong></a></button>
    </div>




</form>
</div>
<div class="col-md-2 "> </div>
</div>


<script type="text/javascript">
    $("#estado_rep").val("<?php echo $repuesto->estado_rep; ?>");
</script>
