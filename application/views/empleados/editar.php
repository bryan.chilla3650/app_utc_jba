<div class="row">
<div class="col-md-2 "> </div>
<div class="col-md-8">
  <center>
    <h1> Editar Empleado</h1>
    <br>
    <hr>
    <br>
</center>
<form action="<?php echo site_url(); ?>/empleados/procesarActualizacion" method="post">

<input class="form-control" type="hidden"  readonly name="id_emp" id="id_emp" value="<?php echo $empleado->id_emp; ?>">
    <br>
    <br>
    <label  for="">IDENTIFICACIÓN</label>
    <input class="form-control"type="number" name="identificador_emp" id="identificador_emp" placeholder="Por favor Ingrese la Identificacion" value="<?php echo $empleado->identificador_emp; ?>">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" type="text" name="nombre_emp" id="nombre_emp" placeholder="Por favor Ingrese el nombre"value="<?php echo $empleado->nombre_emp; ?>">
    <br>
    <br>
    <label  for="">APELLIDO</label>
    <input class="form-control" type="text" name="apellido_emp" id="apellido_emp" placeholder="Por favor Ingrese el apellido" value="<?php echo $empleado->apellido_emp; ?>">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" type="text" name="direccion_emp" id="direccion_emp" placeholder="Por favor Ingrese la dirección"value="<?php echo $empleado->direccion_emp; ?>">
    <br>
    <br>
    <label for="">ESTADO</label>

    <select class="form-control" name="estado_emp" id="estado_emp">
    <option value="">Selecione...</option>
        <option value="Activo">Activo</option>
        <option value="Inactivo">Inactivo</option>
    </select>

    <br>
    <br>
    <div align="center" >
    <button class="btn btn-success" type="submit" name="button"> <i class="fa-solid fa-floppy-disk"></i>
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp
    <button class="btn btn-danger" type="submit" name="button">
    <a href="<?php echo site_url();?>/empleados/index"> <strong style="color:white;"> <i class="fa fa-times"></i> CANCELAR</strong></a></button>
    </div>




</form>
</div>
<div class="col-md-2 "> </div>
</div>


<script type="text/javascript">
    $("#estado_emp").val("<?php echo $empleado->estado_emp; ?>");
</script>
