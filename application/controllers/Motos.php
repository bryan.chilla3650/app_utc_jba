<?php
//para llamar alos controladores y que imprima en la pantalla
 class motos extends CI_controller{
  public function __construct(){
    parent::__construct();
    //para cargar el modelo
    $this->load->model('moto');
 }
 public function  index(){
   //el devuelve el listado en la vista
$data["listadoMotos"]=$this->moto->consultarTodos();
$this->load->view('header');
$this->load->view('motos/index',$data);
$this->load->view('footer');

 }
 public function indexx(){
   $data["listadoMotos"]=$this->moto->consultarTodos();
     $this->load->view('header');
     $this->load->view('motos/indexx',$data);
     $this->load->view('footer');
 }
 public function urbana(){
   $this->load->view('header');
   $this->load->view('motos/urbana');
   $this->load->view('footer');
 }
 public function guardarMoto(){
$datosNuevoMoto=array(
  "nombre_mot"=>$this->input->post("nombre_mot"),
  "email_mot"=>$this->input->post("email_mot"),
  "region_mot"=>$this->input->post("region_mot"),
  "color_mot"=>$this->input->post("color_mot"),
  "telefono_mot"=>$this->input->post("telefono_mot")
);
if($this->moto->insertar($datosNuevoMoto)){
  $this->session->set_flashdata("confirmacion","Cotizacion enviada correctamente.");

 } else {
 $this->session->set_flashdata("error","error el enviar intente nuevamente.");
 }
 redirect("motos/indexx");
}
function procesarEliminacion($id_moto){
      if ($this->moto->eliminar($id_moto)) {
        $this->session->set_flashdata("eliminacion","los datos se han eliminado correctamente.");
     } else {
       $this->session->set_flashdata("error1","error al eliminar intente nuevamente.");
     }
     redirect("motos/indexx");
}

function editar($id_moto){
$data["moto"] = $this->moto->consultarId($id_moto);
     $this->load->view('header');
     $this->load->view('motos/editar',$data);
     $this->load->view('footer');
}

public function procesarActualizacion(){
   $id_moto=$this->input->post('id_moto');
   $datosMotoActualizado=array(
     "nombre_mot"=>$this->input->post("nombre_mot"),
     "email_mot"=>$this->input->post("email_mot"),
     "region_mot"=>$this->input->post("region_mot"),
     "color_mot"=>$this->input->post("color_mot"),
      "telefono_mot"=>$this->input->post("telefono_mot"),
   );

   if ($this->moto->actualizar($id_moto,$datosMotoActualizado)) {
     $this->session->set_flashdata("actualizacion"," los datos se han actualizado correctamente.");
     }else{
         $this->session->set_flashdata("error2","se datos no se  han actualizado correctamente.");
     }
         redirect("motos/indexx");
   }
 
}
?>
