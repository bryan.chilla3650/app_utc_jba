<?php
//para llamar alos controladores y que imprima en la pantalla
 class camiones extends CI_controller{
  public function __construct(){
    parent::__construct();
    //para cargar el modelo
    $this->load->model('camion');
 }
 public function  index(){
   //el devuelve el listado en la vista
$data["listadoCamiones"]=$this->camion->consultarTodos();
$this->load->view('header');
$this->load->view('camiones/index',$data);
$this->load->view('footer');

 }

 public function pesado(){
   $this->load->view('header');
   $this->load->view('camiones/pesado');
   $this->load->view('footer');
 }
 public function guardarCamion(){
$datosNuevoCamion=array(
  "nombre_cam"=>$this->input->post("nombre_cam"),
  "marca_cam"=>$this->input->post("marca_cam"),
  "tonelaje_cam"=>$this->input->post("tonelaje_cam"),
  "color_cam"=>$this->input->post("color_cam"),
  "tipo_cam"=>$this->input->post("tipo_cam")
);
if($this->camion->insertar($datosNuevoCamion)){
  echo " resgistrado exitoso";

} else{
  echo "error al registrarse";
}
}
//procesar el boton eliminación
  function procesarEliminacion($id_cam){
  if($this->camion->eliminar($id_cam)){
    redirect("camiones/index");
  }else{
    echo "Error al eliminar";
  }
}
}
?>
