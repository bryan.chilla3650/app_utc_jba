<?php
//para llamar alos controladores y que imprima en la pantalla
 class Cabinas extends CI_controller{
  public function __construct(){
    parent::__construct();
    //para cargar el modelo
    $this->load->model('cabina');
 }
 public function  index(){
   //el devuelve el listado en la vista
$data["listadoCabinas"]=$this->cabina->consultarTodos();
$this->load->view('header');
$this->load->view('cabinas/index',$data);
$this->load->view('footer');

 }


   public function editar($id_cabina){
     $data ['listadoCabinas']=$this->cabina->consultarTodos();
     $data['cabina']=$this->cabina->consultarPorId($id_cabina);
     $this->load->view("header");
     $this->load->view("cabinas/editar",$data);
     $this->load->view("footer");
   }


 public function metal(){
   $this->load->view('header');
   $this->load->view('cabinas/metal');
   $this->load->view('footer');
 }
 public function guardarCabina(){
$datosNuevoCabina=array(
  "nombre_cab"=>$this->input->post("nombre_cab"),
  "marca_cab"=>$this->input->post("marca_cab"),
  "color_cab"=>$this->input->post("color_cab"),
  "tipo_cab"=>$this->input->post("tipo_cab")
);
if($this->cabina->insertar($datosNuevoCabina)){
  echo " resgistrado exitoso";

} else{
  echo "error al registrarse";
}
}
//procesar el boton eliminación
  function procesarEliminacion($id_cabina){
  if($this->cabina->eliminar($id_cabina)){
    redirect("cabinas/index");
  }else{
    echo "Error al eliminar";
  }
}

}
?>
