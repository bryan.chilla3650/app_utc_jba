<?php

    class Vehiculos extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model('vehiculo');
        }

        public function index(){
          $data["listadoVehiculos"]=$this->vehiculo->consultarTodos();
            $this->load->view('header');
            $this->load->view('vehiculos/index',$data);
            $this->load->view('footer');
        }

        public function nuevo(){
            $this->load->view('header');
            $this->load->view('vehiculos/nuevo');
            $this->load->view('footer');
        }

        public function guardarVehiculo(){
          $datosNuevoVehiculo=array(
              "identificador_veh"=>$this->input->post("identificador_veh"),
              "nombre_veh"=>$this->input->post("nombre_veh"),
              "color_veh"=>$this->input->post("color_veh"),
              "tipo_veh"=>$this->input->post("tipo_veh"),
              "estado_veh"=>$this->input->post("estado_veh")
            );

            if ($this->vehiculo->insertar($datosNuevoVehiculo)) {
              $this->session->set_flashdata("confirmacion","Vehiculo insertado exitosamente.");
            } else {
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
            }
            redirect("vehiculos/index");
        }

          function procesarEliminacion($id_veh){
                if ($this->vehiculo->eliminar($id_veh)) {
                  redirect("vehiculos/index");
                } else {
                  echo "ERROR AL ELIMINAR";
                }
        }

        function editar($id_veh){
          $data["vehiculo"] = $this->vehiculo->consultarId($id_veh);
                $this->load->view('header');
                $this->load->view('vehiculos/editar',$data);
                $this->load->view('footer');
        }

        public function procesarActualizacion(){
              $id_veh=$this->input->post('id_veh');
              $datosVehiculoActualizado=array(
                "identificador_veh"=>$this->input->post("identificador_veh"),
                "nombre_veh"=>$this->input->post("nombre_veh"),
                "color_veh"=>$this->input->post("color_veh"),
                "tipo_veh"=>$this->input->post("tipo_veh"),
                "estado_veh"=>$this->input->post("estado_veh")
              );

              if ($this->vehiculo->actualizar($id_veh, $datosVehiculoActualizado)) {
                $this->session->set_flashdata("confirmacion","Vehiculo Actualizado exitosamente.");
              }else{
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
              }
              redirect("vehiculos/index");
            }

    }

 ?>
