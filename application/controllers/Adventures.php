<?php
//para llamar alos controladores y que imprima en la pantalla
 class Adventures extends CI_controller{
  public function __construct(){
    parent::__construct();
    //para cargar el modelo
    $this->load->model('adventure');
 }
 public function  index(){
   //el devuelve el listado en la vista
$data["listadoAdventures"]=$this->adventure->consultarTodos();
$this->load->view('header');
$this->load->view('adventures/index',$data);
$this->load->view('footer');

 }
 public function indexx(){
   $data["listadoAdventures"]=$this->adventure->consultarTodos();
     $this->load->view('header');
     $this->load->view('adventures/indexx',$data);
     $this->load->view('footer');
 }

 public function fo(){
   $this->load->view('header');
   $this->load->view('adventures/fo');
   $this->load->view('footer');
 }
 public function guardarAdventure(){
   $datosNuevoAdventure=array(

       "nombre_ad"=>$this->input->post("nombre_ad"),
       "apellido_ad"=>$this->input->post("apellido_ad"),
       "telefono_ad"=>$this->input->post("telefono_ad"),
       "email_ad"=>$this->input->post("email_ad"),
        "ciudad_ad"=>$this->input->post("ciudad_ad"),
         "agencia_ad"=>$this->input->post("agencia_ad")
     );

     if ($this->adventure->insertar($datosNuevoAdventure)) {
       $this->session->set_flashdata("confirmacion","Cotizacion enviada correctamente.");

     } else {
     $this->session->set_flashdata("error","error el enviar intente nuevamente.");
     }
      redirect("adventures/indexx");
 }

   function procesarEliminacion($id_ad){
         if ($this->adventure->eliminar($id_ad)) {
            $this->session->set_flashdata("eliminacion","los datos se han eliminado correctamente.");
         } else {
           $this->session->set_flashdata("error1","error al eliminar intente nuevamente.");
         }
         redirect("adventures/indexx");
 }

public function editar($id_ad){
  $data["adventure"] = $this->adventure->consultarId($id_ad);
        $this->load->view('header');
        $this->load->view('adventures/editar',$data);
        $this->load->view('footer');
}

      public function procesarActualizacion(){
      $id_ad=$this->input->post('id_ad');
      $datosAdventureActualizado=array(
        "nombre_ad"=>$this->input->post("nombre_ad"),
        "apellido_ad"=>$this->input->post("apellido_ad"),
        "telefono_ad"=>$this->input->post("telefono_ad"),
        "email_ad"=>$this->input->post("email_ad"),
         "ciudad_ad"=>$this->input->post("ciudad_ad"),
          "agencia_ad"=>$this->input->post("agencia_ad"),
      );

      if ($this->adventure->actualizar($id_ad,$datosAdventureActualizado)) {
      $this->session->set_flashdata("actualizacion"," los datos se han actualizado correctamente.");
      }else{
          $this->session->set_flashdata("error2","se datos no se  han actualizado correctamente.");
      }
          redirect("adventures/indexx");
    }
}
?>
