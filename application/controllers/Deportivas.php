<?php
//para llamar alos controladores y que imprima en la pantalla
 class deportivas extends CI_controller{
  public function __construct(){
    parent::__construct();
    //para cargar el modelo deportiva
    $this->load->model('deportiva');
 }
 public function  index(){
   //el devuelve el listado en la vista
$data["listadoDeportivas"]=$this->deportiva->consultarTodos();
$this->load->view('header');
$this->load->view('deportivas/index',$data);
$this->load->view('footer');

 }
 public function indexx(){
   $data["listadoDeportivas"]=$this->deportiva->consultarTodos();
     $this->load->view('header');
     $this->load->view('deportivas/indexx',$data);
     $this->load->view('footer');
 }

 public function formu(){
   $this->load->view('header');
   $this->load->view('deportivas/formu');
   $this->load->view('footer');
 }
 public function guardarDeportivas(){
$datosNuevoDeportiva=array(
  "nombre_depo"=>$this->input->post("nombre_depo"),
  "apellido_depo"=>$this->input->post("apellido_depo"),
  "telefono_depo"=>$this->input->post("telefono_depo"),
  "ciudad_depo"=>$this->input->post("ciudad_depo"),
  "agencia_depo"=>$this->input->post("agencia_depo")
);
if($this->deportiva->insertar($datosNuevoDeportiva)){
  $this->session->set_flashdata("confirmacion","Cotizacion enviada correctamente.");

} else {
$this->session->set_flashdata("error","error el enviar intente nuevamente.");
}
 redirect("deportivas/indexx");
}
}
function procesarEliminacion($id_depo){
      if ($this->deportiva->eliminar($id_depo)) {
        $this->session->set_flashdata("eliminacion","los datos se han eliminado correctamente.");
     } else {
       $this->session->set_flashdata("error1","error al eliminar intente nuevamente.");
     }
     redirect("deportivas/indexx");
      }
}

function editar($id_depo){
$data["deportiva"] = $this->deportiva->consultarId($id_depo);
     $this->load->view('header');
     $this->load->view('deportivas/editar',$data);
     $this->load->view('footer');
}

public function procesarActualizacion(){
   $id_depo=$this->input->post('id_depo');
   $datosDeportivaActualizado=array(
     "nombre_depo"=>$this->input->post("nombre_depo"),
     "apellido_depo"=>$this->input->post("apellido_depo"),
     "telefono_depo"=>$this->input->post("telefono_depo"),
     "email_depo"=>$this->input->post("email_depo"),
      "ciudad_depo"=>$this->input->post("ciudad_depo"),
        "agencia_depo"=>$this->input->post("agencia_depo"),
   );

   if ($this->deportiva->actualizar($id_depo,$datosDeportivaActualizado)) {
     $this->session->set_flashdata("actualizacion"," los datos se han actualizado correctamente.");
     }else{
         $this->session->set_flashdata("error2","se datos no se  han actualizado correctamente.");
     }
         redirect("adventures/indexx");
   }
 }




}
?>
