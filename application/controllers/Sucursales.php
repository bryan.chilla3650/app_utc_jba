<?php

    class Sucursales extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model('sucursal');
        }

        public function index(){
          $data["listadoSucursal"]=$this->sucursal->consultarTodos();
            $this->load->view('header');
            $this->load->view('sucursales/index',$data);
            $this->load->view('footer');
        }

        public function nuevo(){
            $this->load->view('header');
            $this->load->view('sucursales/nuevo');
            $this->load->view('footer');
        }

        public function guardarSucursal(){
          $datosNuevaSucursal=array(
              "identificador_suc"=>$this->input->post("identificador_suc"),
              "nombre_suc"=>$this->input->post("nombre_suc"),
              "encargado_suc"=>$this->input->post("encargado_suc"),
              "direccion_suc"=>$this->input->post("direccion_suc"),
              "estado_suc"=>$this->input->post("estado_suc")
            );

            if ($this->sucursal->insertar($datosNuevaSucursal)) {
              $this->session->set_flashdata("confirmacion","Sucursal insertado exitosamente.");
            } else {
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
            }
            redirect("sucursales/index");
        }

          function procesarEliminacion($id_suc){
                if ($this->sucursal->eliminar($id_suc)) {
                  redirect("sucursales/index");
                } else {
                  echo "ERROR AL ELIMINAR";
                }
        }

        function editar($id_suc){
          $data["sucursal"] = $this->sucursal->consultarId($id_suc);
                $this->load->view('header');
                $this->load->view('sucursales/editar',$data);
                $this->load->view('footer');
        }

        public function procesarActualizacion(){
              $id_suc=$this->input->post('id_suc');
              $datoSucursalActualizado=array(
                "identificador_suc"=>$this->input->post("identificador_suc"),
                "nombre_suc"=>$this->input->post("nombre_suc"),
                "encargado_suc"=>$this->input->post("encargado_suc"),
                "direccion_suc"=>$this->input->post("direccion_suc"),
                "estado_suc"=>$this->input->post("estado_suc")
              );

              if ($this->sucursal->actualizar($id_suc, $datoSucursalActualizado)) {
                $this->session->set_flashdata("confirmacion","Sucursal Actualizado exitosamente.");
              }else{
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
              }
              redirect("sucursales/index");
            }

    }

 ?>
