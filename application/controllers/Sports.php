<?php
//para llamar alos controladores y que imprima en la pantalla
 class sports extends CI_controller{
  public function __construct(){
    parent::__construct();
    //para cargar el modelo
    $this->load->model('sport');
 }
 public function  index(){
   //el devuelve el listado en la vista
$data["listadoSports"]=$this->sport->consultarTodos();
$this->load->view('header');
$this->load->view('sports/index',$data);
$this->load->view('footer');

 }
 public function indexx(){
   $data["listadoSports"]=$this->sport->consultarTodos();
     $this->load->view('header');
     $this->load->view('sports/indexx',$data);
     $this->load->view('footer');
 }
 public function f(){
   $this->load->view('header');
   $this->load->view('sports/f');
   $this->load->view('footer');
 }
 public function guardarSport(){
$datosNuevoSport=array(
  "nombre_sp"=>$this->input->post("nombre_sp"),
  "apellido_sp"=>$this->input->post("apellido_sp"),
  "telefono_sp"=>$this->input->post("telefono_sp"),
  "email_sp"=>$this->input->post("email_sp"),
  "ciudad_sp"=>$this->input->post("ciudad_sp"),
  "agencia_sp"=>$this->input->post("agencia_sp")
);
if($this->sport->insertar($datosNuevoSport)){
  $this->session->set_flashdata("confirmacion","Cotizacion enviada correctamente.");

} else {
$this->session->set_flashdata("error","error el enviar intente nuevamente.");
}
redirect("sports/indexx");
}
function procesarEliminacion($id_sp){
      if ($this->sport->eliminar($id_sp)) {
        $this->session->set_flashdata("eliminacion","los datos se han eliminado correctamente.");
     } else {
       $this->session->set_flashdata("error1","error al eliminar intente nuevamente.");
     }
     redirect("sports/indexx");
      }
}

function editar($id_sp){
$data["sport"] = $this->sport->consultarId($id_sp);
     $this->load->view('header');
     $this->load->view('sports/editar',$data);
     $this->load->view('footer');
}

public function procesarActualizacion(){
   $id_sp=$this->input->post('id_sp');
   $datosSportActualizado=array(
     "nombre_sp"=>$this->input->post("nombre_sp"),
     "apellido_sp"=>$this->input->post("apellido_sp"),
     "telefono_sp"=>$this->input->post("telefono_sp"),
     "email_sp"=>$this->input->post("email_sp"),
      "ciudad_sp"=>$this->input->post("ciudad_sp"),
        "agencia_sp"=>$this->input->post("agencia_sp"),
   );

   if ($this->sport->actualizar($id_sp,$datosSportActualizado)) {
     $this->session->set_flashdata("actualizacion"," los datos se han actualizado correctamente.");
     }else{
         $this->session->set_flashdata("error2","se datos no se  han actualizado correctamente.");
     }
         redirect("sports/indexx");
   }
 }



}
?>
