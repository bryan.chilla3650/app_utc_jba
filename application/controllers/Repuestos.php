<?php

    class Repuestos extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model('repuesto');
        }

        public function index(){
          $data["listadoRepuestos"]=$this->repuesto->consultarTodos();
            $this->load->view('header');
            $this->load->view('repuestos/index',$data);
            $this->load->view('footer');
        }

        public function nuevo(){
            $this->load->view('header');
            $this->load->view('repuestos/nuevo');
            $this->load->view('footer');
        }

        public function guardarRepuestos(){
          $datosNuevoRepuesto=array(
              "identificador_rep"=>$this->input->post("identificador_rep"),
              "nombre_rep"=>$this->input->post("nombre_rep"),
              "descripcion_rep"=>$this->input->post("descripcion_rep"),
              "estado_rep"=>$this->input->post("estado_rep")
            );

            if ($this->repuesto->insertar($datosNuevoRepuesto)) {
                $this->session->set_flashdata("confirmacion","Repuesto insertado exitosamente.");
            } else {
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
            }
            redirect("repuestos/index");
        }

          function procesarEliminacion($id_cli){
                if ($this->repuesto->eliminar($id_cli)) {
                  redirect("clientes/index");
                } else {
                  echo "ERROR AL ELIMINAR";
                }
        }

        function editar($id_rep){
          $data["repuesto"] = $this->repuesto->consultarId($id_rep);
                $this->load->view('header');
                $this->load->view('repuestos/editar',$data);
                $this->load->view('footer');
        }

        public function procesarActualizacion(){
              $id_rep=$this->input->post('id_rep');
              $datosRepuestoActualizado=array(
                "identificador_rep"=>$this->input->post("identificador_rep"),
                "nombre_rep"=>$this->input->post("nombre_rep"),
                "descripcion_rep"=>$this->input->post("descripcion_rep"),
                "estado_rep"=>$this->input->post("estado_rep")
              );

              if ($this->repuesto->actualizar($id_rep, $datosRepuestoActualizado)) {
                $this->session->set_flashdata("confirmacion","Repuesto Actualizado exitosamente.");
              }else{
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
              }
              redirect("repuestos/index");
            }

    }

 ?>
