<?php
//para la base de datos
class Furgon extends CI_model{
  public function __construct() {
    parent::__construct();
  }
  public function insertar($datos){
//para insertar datos en la base de datos
  return $this->db->insert('furgon',$datos);
  }
  public function consultarTodos(){
//para consultar
    $listadoFurgones=$this->db->get('furgon');
    //para validar consultar clientes
    if ($listadoFurgones->num_rows()>0){
      //para que consulte si hay clientes
      return $listadoFurgones;
    } else
    {
//al no tener clientes devuelve
      return false;
    }
  }
  public function eliminar($id_fur){
    $this->db->where("id_fur",$id_fur);
    return $this->db->delete("furgon");
  }
}
 ?>
