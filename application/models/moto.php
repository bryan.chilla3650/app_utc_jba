<?php
//para la base de datos
class Moto extends CI_model{
  public function __construct() {
    parent::__construct();
  }
  public function insertar($datos){
//para insertar datos en la base de datos
  return $this->db->insert('moto',$datos);
  }
  public function consultarTodos(){
//para consultar
    $listadoMotos=$this->db->get('moto');
    //para validar consultar clientes
    if ($listadoMotos->num_rows()>0){
      //para que consulte si hay clientes
      return $listadoMotos;
    } else
    {
//al no tener clientes devuelve
      return false;
    }
  }

    //eliminar
    public function eliminar($id_moto){
        $this->db->where("id_moto",$id_moto);
        return $this->db->delete("moto");
    }

     //para hacer la consulta de la bdb en la vista
     public function consultarId($id_moto){
         $this->db->where("id_moto",$id_moto);
         $listadoMotos=$this->db->get('moto');
         if ($listadoMotos->num_rows()>0) {
           //clientes
           return $listadoMotos->row();
         }else {
           // sin datos
           return false;
         }
       }
       //actualizar
            public function actualizar($id_moto,$datos){
              $this->db->where("id_moto",$id_moto);
              return $this->db->update("moto",$datos);
            }


}
 ?>
