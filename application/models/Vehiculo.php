<?php
  class Vehiculo extends CI_Model{
    public function __construct(){
        parent:: __construct();
  }
  public function insertar($datos){
      return $this->db->insert('vehiculo',$datos);
  }

    public function consultarTodos(){
      $listadoVehiculos=$this->db->get('vehiculo');
      if ($listadoVehiculos->num_rows()>0) {
        return $listadoVehiculos;
      } else {
        return false;
      }
    }

    public function eliminar($id_veh){
        $this->db->where("id_veh",$id_veh);
        return $this->db->delete("vehiculo");
    }
            //MODIFIC
    public function consultarId($id_veh){
        $this->db->where("id_veh",$id_veh);
        $listadoVehiculos=$this->db->get('vehiculo');
        if ($listadoVehiculos->num_rows()>0) {
          //clientes
          return $listadoVehiculos->row();
        }else {
          // sin datos
          return false;
        }
       }
       public function actualizar($id_veh,$datos){
         $this->db->where("id_veh",$id_veh);
         return $this->db->update("vehiculo",$datos);
       }
  }

 ?>
