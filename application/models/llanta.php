<?php
//para la base de datos
class Llanta extends CI_model{
  public function __construct() {
    parent::__construct();
  }
  public function insertar($datos){
//para insertar datos en la base de datos
  return $this->db->insert('llanta',$datos);
  }
  public function consultarTodos(){
//para consultar
    $listadoLlantas=$this->db->get('llanta');
    //para validar consultar clientes
    if ($listadoLlantas->num_rows()>0){
      //para que consulte si hay clientes
      return $listadoLlantas;
    } else
    {
//al no tener clientes devuelve
      return false;
    }
  }
  public function eliminar($id_llanta){
    $this->db->where("id_llanta",$id_llanta);
    return $this->db->delete("llanta");
  }
}
 ?>
