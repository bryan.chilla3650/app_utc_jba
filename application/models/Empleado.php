<?php
  class Empleado extends CI_Model{
    public function __construct(){
        parent:: __construct();
  }
  public function insertar($datos){
      return $this->db->insert('empleado',$datos);
  }

    public function consultarTodos(){
      $listadoEmpleado=$this->db->get('empleado');
      if ($listadoEmpleado->num_rows()>0) {
        return $listadoEmpleado;
      } else {
        return false;
      }
    }

    public function eliminar($id_emp){
        $this->db->where("id_emp",$id_emp);
        return $this->db->delete("empleado");
    }
            //MODIFIC
    public function consultarId($id_emp){
        $this->db->where("id_emp",$id_emp);
        $listadoEmpleado=$this->db->get('empleado');
        if ($listadoEmpleado->num_rows()>0) {
          //clientes
          return $listadoEmpleado->row();
        }else {
          // sin datos
          return false;
        }
       }
       public function actualizar($id_emp,$datos){
         $this->db->where("id_emp",$id_emp);
         return $this->db->update("empleado",$datos);
       }
  }

 ?>
