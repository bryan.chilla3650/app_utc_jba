<?php
  class Repuesto extends CI_Model{
    public function __construct(){
        parent:: __construct();
  }
  public function insertar($datos){
      return $this->db->insert('repuesto',$datos);
  }

    public function consultarTodos(){
      $listadoRepuestos=$this->db->get('repuesto');
      if ($listadoRepuestos->num_rows()>0) {
        return $listadoRepuestos;
      } else {
        return false;
      }
    }

    public function eliminar($id_rep){
        $this->db->where("id_rep",$id_rep);
        return $this->db->delete("repuesto");
    }
            //MODIFIC
    public function consultarId($id_rep){
        $this->db->where("id_rep",$id_rep);
        $listadoRepuestos=$this->db->get('repuesto');
        if ($listadoRepuestos->num_rows()>0) {
          //clientes
          return $listadoRepuestos->row();
        }else {
          // sin datos
          return false;
        }
       }
       public function actualizar($id_rep,$datos){
         $this->db->where("id_rep",$id_rep);
         return $this->db->update("repuesto",$datos);
       }
  }

 ?>
