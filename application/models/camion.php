<?php
//para la base de datos
class Camion extends CI_model{
  public function __construct() {
    parent::__construct();
  }
  public function insertar($datos){
//para insertar datos en la base de datos
  return $this->db->insert('camion',$datos);
  }
  public function consultarTodos(){
//para consultar
    $listadoCamiones=$this->db->get('camion');
    //para validar consultar clientes
    if ($listadoCamiones->num_rows()>0){
      //para que consulte si hay clientes
      return $listadoCamiones;
    } else
    {
//al no tener clientes devuelve
      return false;
    }
  }
  public function eliminar($id_cam){
    $this->db->where("id_cam",$id_cam);
    return $this->db->delete("camion");
  }
}
 ?>
