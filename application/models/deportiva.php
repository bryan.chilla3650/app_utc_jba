<?php
//para la base de datos
class Deportiva extends CI_model{
  public function __construct() {
    parent::__construct();
  }
  public function insertar($datos){
//para insertar datos en la base de datos
  return $this->db->insert('deportiva',$datos);
  }
  public function consultarTodos(){
//para consultar
    $deportivas=$this->db->get('deportiva');
    //para validar consultar clientes
    if ($deportivas->num_rows()>0){
      //para que consulte si hay clientes
      return $deportivas;
    } else
    {
//al no tener clientes devuelve
      return false;
    }
  }
  //eliminar
  public function eliminar($id_depo){
      $this->db->where("id_depo",$id_depo);
      return $this->db->delete("deportiva");
  }

   //para hacer la consulta de la bdb en la vista
   public function consultarId($id_depo){
       $this->db->where("id_depo",$id_depo);
       $listadoDeportivas=$this->db->get('deportiva');
       if ($listadoDeportivas->num_rows()>0) {
         //clientes
         return $listadoDeportivas->row();
       }else {
         // sin datos
         return false;
       }
     }
     //actualizar
          public function actualizar($id_depo,$datos){
            $this->db->where("id_depo",$id_depo);
            return $this->db->update("deportiva",$datos);
          }







}
 ?>
