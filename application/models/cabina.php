<?php
//para la base de datos
class Cabina extends CI_model{
  public function __construct() {
    parent::__construct();
  }
  public function insertar($datos){
//para insertar datos en la base de datos
  return $this->db->insert('cabina',$datos);
  }
  public function consultarTodos(){
//para consultar
    $listadoCabinas=$this->db->get('cabina');
    //para validar consultar clientes
    if ($listadoCabinas->num_rows()>0){
      //para que consulte si hay clientes
      return $listadoCabinas;
    } else
    {
//al no tener clientes devuelve
      return false;
    }
  }
  public function eliminar($id_cabina){
    $this->db->where("id_cabina",$id_cabina);
    return $this->db->delete("cabina");
  }

}
 ?>
