<?php
//para la base de datos
class Adventure extends CI_model{
  public function __construct() {
    parent::__construct();
  }
  public function insertar($datos){
//para insertar datos en la base de datos
  return $this->db->insert('adventure',$datos);
  }
  public function consultarTodos(){
//para consultar
    $adventure=$this->db->get('adventure');
    //para validar consultar clientes
    if ($adventure->num_rows()>0){
      //para que consulte si hay clientes
      return $adventure;
    } else
    {
//al no tener clientes devuelve
      return false;
    }
  }

    //eliminar
    public function eliminar($id_ad){
        $this->db->where("id_ad",$id_ad);
        return $this->db->delete("adventure");
    }

     //para hacer la consulta de la bdb en la vista
     public function consultarId($id_ad){
         $this->db->where("id_ad",$id_ad);
         $listadoAdventures=$this->db->get('adventure');
         if ($listadoAdventures->num_rows()>0) {
           //clientes
           return $listadoAdventures->row();
         }else {
           // sin datos
           return false;
         }
       }
       //actualizar
            public function actualizar($id_ad,$datos){
              $this->db->where("id_ad",$id_ad);
              return $this->db->update("adventure",$datos);
            }


}
 ?>
